<?php

namespace BitkornCalendar\Controller;

use Zend\Mvc\Controller\AbstractActionController;

/**
 *
 * @author allapow
 */
class AbstractController extends AbstractActionController
{

    /**
     * @var \Zend\ServiceManager\ServiceLocatorInterface
     */
    protected $serviceLocator;

    public function setServiceLocator(\Zend\ServiceManager\ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    /**
     *
     * @var \Zend\Log\Logger
     */
    protected $logger;

    /**
     * 
     * @param \Zend\Log\Logger $logger
     */
    public function setLogger(\Zend\Log\Logger $logger)
    {
        $this->logger = $logger;
    }
        
    /**
     * 
     * @return \Zend\Log\Logger
     */
    protected function getLogger()
    {
        if (!isset($this->logger) || !($this->logger instanceof \Zend\Log\Logger)) {
            $this->logger = $this->serviceLocator->get('logger');
        }
        return $this->logger;
    }

}
