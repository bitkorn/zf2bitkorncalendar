<?php

namespace BitkornCalendar\Controller\Presentation;

use BitkornCalendar\Controller\AbstractController;
use Zend\View\Model\ViewModel;
use BitkornCalendar\Calendar\Month;
use BitkornCalendar\Calendar\Day;
use BitkornCalendar\Concrete\Calendar\DayCustom;
use BitkornCalendar\Attachment\BaseAttachment;
use BitkornCalendar\Term\PeriodMonth;
use BitkornCalendar\Term\PeriodDay;
use BitkornCalendar\Render\Month\MonthTableRenderer;
use BitkornCalendar\Render\Month\MultiMonthTableRenderer;

/**
 * 
 *
 * @author allapow
 */
class MonthController extends AbstractController
{

    public function monthPresentationAction()
    {
        $viewModel = new ViewModel();

        $month = new Month(8, 2018);
//        $month->setLogger($this->logger);

        $month->setCustomDayClass(DayCustom::class);

        $periodDay = new PeriodDay('2018-07-12', '2018-08-04');
        $attachmentPd01 = new BaseAttachment('a1');
        $attachmentPd01->replaceViewHtml(function($viewHtml) {
            return '<div class="term-header">Wanderwochen 01</div>';
        });
        $periodDay->setAttachment($attachmentPd01);
        $periodDay->init();
        $month->addPeriodDay($periodDay);

        $periodDay02 = new PeriodDay('2018-08-12', '2018-09-04');
        $attachmentPd02 = new BaseAttachment('a2');
        $attachmentPd02->replaceViewHtml(function($viewHtml) {
            return '<div class="term-header">Wanderwochen 02</div>';
        });
        $periodDay02->setAttachment($attachmentPd02);
        $periodDay02->init();
        $month->addPeriodDay($periodDay02);

        $attachment01 = new BaseAttachment('c4');
        $someZeug = 'zeug';
//        $replaceFunc = function($viewHtml){ return 'my own HTML';};
        $attachment01->replaceViewHtml(function($viewHtml) use ($someZeug) {
            return 'with ' . $someZeug;
        });
        $month->addAttachment('2018-08-03', $attachment01);

        $month->computeMonthGrid();

        $monthRenderer = new MonthTableRenderer($month);

        $viewModel->setVariable('month', $monthRenderer);

        return $viewModel;
    }

    public function multiMonthPresentationAction()
    {
        $viewModel = new ViewModel();

        $startIso8601 = $this->params('start', '2018-07-12');
        $endIso8601 = $this->params('end', '2018-09-22');

        $periodDay = new PeriodDay('2018-07-08', '2018-08-04');
        $attachmentPd01 = new BaseAttachment('a1');
        $attachmentPd01->replaceViewHtml(function($viewHtml) {
            return '<div class="term-header">Wanderwochen 01</div>';
        });
        $periodDay->setAttachment($attachmentPd01);
        $periodDay->init();

        $periodDay02 = new PeriodDay('2018-08-12', '2018-09-04');
        $attachmentPd02 = new BaseAttachment('a2');
        $attachmentPd02->replaceViewHtml(function($viewHtml) {
            return '<div class="term-header">Wanderwochen 02</div>';
        });
        $periodDay02->setAttachment($attachmentPd02);
        $periodDay02->init();


        $periodMonth = new PeriodMonth($startIso8601, $endIso8601);
        $periodMonth->init();
        
        $multiMonthRenderer = new MultiMonthTableRenderer($periodMonth);
        $multiMonthRenderer->addPeriodDay($periodDay);
        $multiMonthRenderer->addPeriodDay($periodDay02);
        $multiMonthRenderer->init();
        $viewModel->setVariable('multiMonthRenderer', $multiMonthRenderer);
        
        /*
         * oder so
         */
//        $multiMonthRenderer = new \BitkornCalendar\Render\MultiMonthRendererAbstract($periodMonth);
//        $multiMonthRenderer->setMonthRendererClass(MonthTableRenderer::class);
//        $multiMonthRenderer->setDayClass(DayCustom::class);
//        $multiMonthRenderer->addPeriodDay($periodDay);
//        $multiMonthRenderer->addPeriodDay($periodDay02);
//        $multiMonthRenderer->init();
//        $viewModel->setVariable('multiMonthRenderer', $multiMonthRenderer);

        return $viewModel;
    }

    public function multiMonthHorizontalPresentationAction()
    {
        $viewModel = new ViewModel();

        $startIso8601 = $this->params('start', '2018-07-12');
        $endIso8601 = $this->params('end', '2018-09-22');

        $periodDay = new PeriodDay('2018-07-08', '2018-08-04');
        $attachmentPd01 = new BaseAttachment('a1');
        $attachmentPd01->replaceViewHtml(function($viewHtml) {
            return '<div class="term-header">WW 01</div>';
        });
        $periodDay->setAttachment($attachmentPd01);
        $periodDay->init();

        $periodDay02 = new PeriodDay('2018-08-12', '2018-09-04');
        $attachmentPd02 = new BaseAttachment('a1');
        $attachmentPd02->replaceViewHtml(function($viewHtml) {
            return '<div class="term-header">WW 02</div>';
        });
        $periodDay02->setAttachment($attachmentPd02);
        $periodDay02->init();


        $periodDayUser01 = new PeriodDay('2018-07-12', '2018-08-02');
        $attachmentUser01 = new BaseAttachment('ua');
        $attachmentUser01->replaceViewHtml(function($viewHtml) {
            return 'torti';
        });
        $periodDayUser01->setAttachment($attachmentUser01);
        $periodDayUser01->init();

        $periodDayUser02 = new PeriodDay('2018-07-08', '2018-07-27');
        $attachmentUser02 = new BaseAttachment('ub');
        $attachmentUser02->replaceViewHtml(function($viewHtml) {
            return 'Alfred';
        });
        $periodDayUser02->setAttachment($attachmentUser02);
        $periodDayUser02->init();

        $periodDayUser03 = new PeriodDay('2018-07-16', '2018-07-29');
        $attachmentUser03 = new BaseAttachment('uc');
        $attachmentUser03->replaceViewHtml(function($viewHtml) {
            return 'Holger';
        });
        $periodDayUser03->setAttachment($attachmentUser03);
        $periodDayUser03->init();

        $periodMonth = new PeriodMonth($startIso8601, $endIso8601);
        $periodMonth->init();
        
        $multiMonthRenderer = new \BitkornCalendar\Render\Month\MultiMonthHorizontalRenderer($periodMonth);
        $multiMonthRenderer->setMonthRendererClass(\BitkornCalendar\Render\MonthRendererAbstract::class);
        $multiMonthRenderer->setDayClass(\BitkornCalendar\Concrete\Calendar\MultiMonthDay::class);
        $multiMonthRenderer->addPeriodDay($periodDay);
        $multiMonthRenderer->addPeriodDay($periodDay02);
        $multiMonthRenderer->addPeriodDay($periodDayUser01);
        $multiMonthRenderer->addPeriodDay($periodDayUser02);
        $multiMonthRenderer->addPeriodDay($periodDayUser03);
        $multiMonthRenderer->init();
        $viewModel->setVariable('multiMonthRenderer', $multiMonthRenderer);
        
//        $this->logger->debug(print_r($multiMonthRenderer->getAttachmentsAll(), true));

        return $viewModel;
    }

}
