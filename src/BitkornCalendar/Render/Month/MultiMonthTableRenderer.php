<?php

namespace BitkornCalendar\Render\Month;

use BitkornCalendar\Calendar\Month;
use BitkornCalendar\Render\Month\MonthTableRenderer;
use BitkornCalendar\Render\MultiMonthRendererAbstract;

/**
 * MultiMonthRendererAbstract with hard coded $monthRendererClass & $dayClass.
 * 
 * @author Torsten Brieskorn
 */
class MultiMonthTableRenderer extends MultiMonthRendererAbstract
{

    public function init()
    {
        foreach ($this->periodMonth as $iso8601) {
            $month = new Month(substr($iso8601, 5, 2), substr($iso8601, 0, 4));
            $month->setCustomDayClass(\BitkornCalendar\Concrete\Calendar\DayCustom::class);
            foreach ($this->attachments as $iso8601attachment => $attachment) {
                $month->addAttachment($iso8601attachment, $attachment);
            }
            $iso8601yearMonth = substr($iso8601, 0, 7);
            if (array_key_exists($iso8601yearMonth, $this->periodDays)) {
                foreach ($this->periodDays[$iso8601yearMonth] as $periodDay) {
                    $month->addPeriodDay($periodDay);
                }
            }

            $month->computeMonthGrid();
            $this->monthRenderers[] = new MonthTableRenderer($month);
        }
    }


}
