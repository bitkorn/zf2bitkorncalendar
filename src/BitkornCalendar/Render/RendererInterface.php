<?php

namespace BitkornCalendar\Render;

/**
 *
 * @author Torsten Brieskorn
 */
interface RendererInterface
{

    public function getHtml(): string;
}
