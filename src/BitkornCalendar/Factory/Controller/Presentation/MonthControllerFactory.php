<?php

namespace BitkornCalendar\Factory\Controller\Presentation;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use BitkornCalendar\Controller\Presentation\MonthController;

/**
 * Description of MonthControllerFactory
 *
 * @author allapow
 */
class MonthControllerFactory implements FactoryInterface {

    public function createService(ServiceLocatorInterface $sli) {
        $ctr = new MonthController();
        $sl = $sli->getServiceLocator();
        $ctr->setServiceLocator($sl);
        $ctr->setServiceLocator($sl);
        $ctr->setLogger($sl->get('logger'));
        return $ctr;
    }

}
