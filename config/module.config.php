<?php

namespace BitkornCalendar;

return array(
    'router' => array(
        'routes' => array(
            'bkcalendar_presentation_month' => array(
                'type' => \Zend\Mvc\Router\Http\Literal::class,
                'options' => array(
                    'route' => '/month-presentation',
                    'defaults' => array(
                        '__NAMESPACE__' => 'BitkornCalendar\Controller\Presentation',
                        'controller' => 'Month',
                        'action' => 'monthPresentation',
                    ),
                ),
            ),
            'bkcalendar_presentation_multimonth' => array(
                'type' => \Zend\Mvc\Router\Http\Literal::class,
                'options' => array(
                    'route' => '/multimonth-presentation',
                    'defaults' => array(
                        '__NAMESPACE__' => 'BitkornCalendar\Controller\Presentation',
                        'controller' => 'Month',
                        'action' => 'multiMonthPresentation',
                    ),
                ),
            ),
            'bkcalendar_presentation_multimonthhorizontal' => array(
                'type' => \Zend\Mvc\Router\Http\Literal::class,
                'options' => array(
                    'route' => '/multimonth-horizontal-presentation',
                    'defaults' => array(
                        '__NAMESPACE__' => 'BitkornCalendar\Controller\Presentation',
                        'controller' => 'Month',
                        'action' => 'multiMonthHorizontalPresentation',
                    ),
                ),
            ),
        ),
    ),
    'service_manager' => array(
        'factories' => array(
//            'BitkornCalendar\Controller\Presentation\Month' => Factory\Controller\Presentation\MonthControllerFactory::class
        ),
        'invokables' => array(
        ),
    ),
    'translator' => array(
//        'locale' => 'de_DE', // BitkornListener->setupLocalization() kann das hier nicht überschreiben
        'translation_file_patterns' => array(
            /*
             * Das hier kann auch in der Module.php->onBootstrap()
             * $this->translator->addTranslationFilePattern($type, $baseDir, $pattern)
             * 
             * zum kategorisieren kann man z.B.:
             * 'pattern'  => 'bitkorn_%s.mo'
             * ...%s ist der language code
             */
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
                'text_domain' => 'bitkorn_calendar'
            ),
        ),
    ),
    'view_manager' => array(
        'template_map' => array(
//            'template/eventMonth' => __DIR__ . '/../view/template/eventMonth.phtml',
//            'template/eventDay' => __DIR__ . '/../view/template/eventDay.phtml',
//            'template/admin/eventMonth' => __DIR__ . '/../view/template/admin/eventMonth.phtml',
//            'template/admin/eventDay' => __DIR__ . '/../view/template/admin/eventDay.phtml',
//            'template/admin/eventYear' => __DIR__ . '/../view/template/admin/eventYear.phtml',
        ),
        'template_path_stack' => array(
            'BitkornCalendar' => __DIR__ . '/../view',
        ),
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
    'bitkorn_calendar' => [
        
    ],
);
